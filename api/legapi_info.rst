Get API information
===================

.. http:get:: /legapi/info/

    Get API information

    **Example request**:

    .. sourcecode:: http

        GET /legapi/info/ HTTP/1.1
        Host: example.com
        Accept: application/json, text/javascript

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: text/javascript

        {
            "api_version": "1.0",
            "api_url_prefix": "api",
        }

    :reqheader Accept: the response content type depends on
                       :mailheader:`Accept` header
    :reqheader Authorization: optional OAuth token to authenticate
    :resheader Content-Type: this depends on :mailheader:`Accept`
                            header of request
    :statuscode 200: no error
    :statuscode 404: information not available

.. warning::

  This request is the first one issued by the CamViewer2 application to the video device. The `api_version` and the `api_url_prefix` are used by all the other methods of the API.

The URL pattern used by the CamViewer2 application for all further requests will be:

::

    /<api_url_prefix>/<api_version>/<method_name>

For example, if your <api_url_prefix> equals "/my/api/" and you're supporting the "1.0" API, the CamViewer2 application will send requests such as:

::

    /my/api/1.0/<method_name>
