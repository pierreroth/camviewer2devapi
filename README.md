**CamViewer2 Devices API is a specification** of the JSON API that is expected from video devices by Legrand CamViewer2 mobile application running on both iOS and Android


Prerequisites
-------------

Install Python and Python installer (PIP) and then run:

    pip install -r requirements.txt

Building documentation
----------------------

To build the documentation:

    make html
