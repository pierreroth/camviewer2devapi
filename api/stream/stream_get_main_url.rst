Get Main stream URL
===================

.. http:get:: /<api_url_prefix>/<api_version>/stream/main/

    Get main stream URL. This URL will be used to open a `RTSP <http://en.wikipedia.org/wiki/Real_Time_Streaming_Protocol>`_ connection.

    **Example request**:

    .. sourcecode:: http

        GET /<api_url_prefix>/<api_version>/stream/main/ HTTP/1.1
        Host: example.com
        Accept: application/json, text/javascript

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: text/javascript

        {
            "url": "/stream/main/",
        }

    :reqheader Accept: the response content type depends on
                       :mailheader:`Accept` header
    :reqheader Authorization: optional OAuth token to authenticate
    :resheader Content-Type: this depends on :mailheader:`Accept`
                            header of request
    :statuscode 200: no error
    :statuscode 404: information not available
