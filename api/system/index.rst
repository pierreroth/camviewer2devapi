System API
==========

The system API let us know what are your system internals

.. toctree::
   :maxdepth: 3

   sys_get_info
   sys_get_health


